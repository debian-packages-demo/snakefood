# snakefood

Python dependency grapher

* https://tracker.debian.org/pkg/snakefood

## License
* http://furius.ca/snakefood/COPYING

## See also
* Not yet [\*snakefood\*](https://pkgs.alpinelinux.org/packages?name=*snakefood*)@AlpineLinux